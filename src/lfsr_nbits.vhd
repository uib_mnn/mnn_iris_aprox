-- autogenerate, lfsr_nbits v1
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
--USE ieee.std_logic_arith.all;
--USE ieee.std_logic_unsigned.all;

--============================================================
-- Design Unit: 
-- Purpose: 
--      Design an LFSR for any bit stream required
-- Authors: 
--      Christiam Franco
-- WEB info: https://en.wikipedia.org/wiki/Linear-feedback_shift_register
---------------------------------------------------------------
-- [INFO]
--
-- 1. GENERICS:
-- 	    GNRC_N_BITS : integer representing Bit width for lfsr
-- 
-- 2. PORTS:
-- 	    Inputs:
--          clk : system clock
--          rst : system reset, active ='1' 
--          portIn_en : eneable module, active = '1'
--          portIn_seed: seed value for lfsr start. applied when rst='1'
--
--	    Outputs:	
--          portOut_lfsr : lfsr out
--				
-- 3. Template:
-- 
-- 	    1. Import Package :
--		    -dont use
--
--  	2. Signals and Constants to declare:
--
--          -- Constants for LFSR
--          constant LFSR_N_BITS : integer := 16; 
--          constant LFSR_SEED : 1;
--
--   	    -- Signals for lfsr_nbits
--          -- Inputs 
--          signal lfsrIn_en : STD_LOGIC:='0'; 
--          -- Outputs
--          signal lfsrOut_lfsr : STD_LOGIC_VECTOR(LFSR_N_BITS-1 downto 0);
--	
-- 	    3. Module Connection:
--	
--          -- Instantiation for lfsr_nbits
--          lfsr_inst : entity work.lfsr_nbits
--          generic map(LFSR_N_BITS)
--          port map (clk, rst, lfsrIn_en, std_logic_vector(to_unsigned(LFSR_SEED, LFSR_N_BITS)), lfsrOut_lfsr);
--
-- 
---------------------------------------------------------------
-- Version		Authors				Date				Changes
-- 0.1 			CCFF            11/01/2019              Initial
-- 0.2          CCFF            29/01/2019              Corregidos los taps
--=============================================================

ENTITY lfsr_nbits IS
    generic (
        GNRC_N_BITS : integer:=2
    );
    PORT(
        -- Inputs
        clk, rst : IN STD_LOGIC; 
        portIn_en : IN STD_LOGIC; 
        portIn_seed: in STD_LOGIC_VECTOR(GNRC_N_BITS-1 downto 0);
        --Outputs
        portOut_lfsr : OUT STD_LOGIC_VECTOR(GNRC_N_BITS-1 downto 0)
    );
END lfsr_nbits;

ARCHITECTURE arch OF lfsr_nbits IS
    -- he dejado que el indice empiece en 1 para compatibilidad con la 
    -- documentacion de wikipedia.
    SIGNAL lfsr: STD_LOGIC_VECTOR(GNRC_N_BITS downto 1) := (others=>'1');

BEGIN
    PROCESS (clk,rst,portIn_seed, portIn_en)       
    BEGIN
        IF (clk'EVENT AND clk='1') THEN        
            IF rst ='1' THEN
                lfsr <= portIn_seed;
            ELSIF (portIn_en='1') THEN
                -- Shift Register
                for i in 1 to GNRC_N_BITS-1 loop 
                    lfsr(i+1) <= lfsr(i);
                end loop;
                -- Taps for lfsr en funcion del numero de bits
                -- de la LFSR. many-to-one implementation. Fuente: 
                -- https://en.wikipedia.org/wiki/Linear-feedback_shift_register
                --
                if GNRC_N_BITS = 4 THEN 
                    lfsr(1) <= lfsr(4) XOR lfsr(3);
                elsif GNRC_N_BITS = 5 THEN
                    lfsr(1) <= lfsr(5) XOR lfsr(3);
                elsif GNRC_N_BITS = 6 THEN
                    lfsr(1) <= lfsr(6) XOR lfsr(5);
                elsif GNRC_N_BITS = 7 THEN
                    lfsr(1) <= lfsr(7) XOR lfsr(6);
                elsif GNRC_N_BITS = 8 THEN
                    lfsr(1) <= lfsr(8) XOR lfsr(6) XOR lfsr(5) XOR lfsr(4);
                elsif GNRC_N_BITS = 9 THEN
                    lfsr(1) <= lfsr(9) XOR lfsr(5);
                elsif GNRC_N_BITS = 10 THEN
                    lfsr(1) <= lfsr(10) XOR lfsr(7);
                elsif GNRC_N_BITS = 11 THEN
                    lfsr(1) <= lfsr(11) XOR lfsr(9);
                elsif GNRC_N_BITS = 12 THEN
                    lfsr(1) <= lfsr(12) XOR lfsr(11) XOR lfsr(10) XOR lfsr(4);
                elsif GNRC_N_BITS = 13 THEN
                    lfsr(1) <= lfsr(13) XOR lfsr(12) XOR lfsr(11) XOR lfsr(8);
                elsif GNRC_N_BITS = 14 THEN
                    lfsr(1) <= lfsr(14) XOR lfsr(13) XOR lfsr(12) XOR lfsr(2);
                elsif GNRC_N_BITS = 15 THEN
                    lfsr(1) <= lfsr(15) XOR lfsr(14);
                elsif GNRC_N_BITS = 16 THEN
                    lfsr(1) <= lfsr(16) XOR lfsr(15) XOR lfsr(13) XOR lfsr(4);                    
                end if;
            END IF;
        END IF;
    END PROCESS;

    portOut_lfsr <=lfsr;
    
END arch;
