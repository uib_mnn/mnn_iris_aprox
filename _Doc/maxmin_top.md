
# Entity: maxmin_top 
- **File**: maxmin_top.vhd

## Diagram
![Diagram](maxmin_top.svg "Diagram")
## Ports

| Port name       | Direction | Type                                                       | Description |
| --------------- | --------- | ---------------------------------------------------------- | ----------- |
| clk             | in        | STD_LOGIC                                                  |             |
| rst             | in        | STD_LOGIC                                                  |             |
| i_en            | in        | std_logic                                                  |             |
| i_x_add         | in        | std_logic_vector(log2ceil(PKG_N_WEIGHTS_TOTAL)-1 downto 0) |             |
| i_x_data        | in        | std_logic_vector(PKG_BIT_PRECISSION-1 downto 0)            |             |
| i_x_or_w_select | in        | std_logic                                                  |             |
| i_x_wren        | in        | std_logic                                                  |             |
| o_result        | out       | std_logic_vector(26 downto 0)                              |             |
| o_done          | out       | std_logic                                                  |             |

## Signals

| Name            | Type                                             | Description |
| --------------- | ------------------------------------------------ | ----------- |
| x_idx           | integer range 0 to PKG_N_WEIGHTS_TOTAL-1         |             |
| maxmin_i_x      | pkg_xi_array_t                                   |             |
| maxmin_i_w      | pkg_w_array_t                                    |             |
| lfsr1_seed      | STD_LOGIC_VECTOR (PKG_BIT_PRECISSION-1 DOWNTO 0) |             |
| lfsr2_seed      | STD_LOGIC_VECTOR (PKG_BIT_PRECISSION-1 DOWNTO 0) |             |
| maxmin_o_result | pkg_y_slv_array_t                                |             |

## Processes
- unnamed: ( clk )

## Instantiations

- maxmin_inst: maxmin_net
