# VLSI Results

Resultados para VLSI, despues de sintesis.
- `sim_post_syn_phys.vcd`: vcd del diseño despues del P&R.
- `sim_rtl.vcd`: vcd del RTL.
- `top_mapped2.qor.rpt`: reporte de are y power.